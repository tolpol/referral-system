Install project:
    a) execute:
        composer update

    b) customise file .env on line 1 in root path:
        DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"

    c) create DataBase with:
        php bin/console doctrine:database:create

    d) execute migrations:
        php app/console doctrine:migrations:migrate
